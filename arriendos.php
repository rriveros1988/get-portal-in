<?php
  date_default_timezone_set('America/Santiago');
  ini_set('display_errors', 'On');
  set_time_limit(7200);
  require('consultas.php');
  // $ruta = 'C:\\xampp\\htdocs\\Git\\portalin\\';
  $ruta = '/var/www/html/generico/portalin';
  $cookie = $ruta . 'cookieTT.txt';

  // Pagina 1
  $ch = curl_init('https://www.portalinmobiliario.com/');
  curl_setopt ($ch, CURLOPT_POST, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
  curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

  $respuesta = curl_exec($ch);

  curl_close($ch);

  // Pagina 2
  $tipos = [];
  $unidad1 = [];
  $unidad1["tipo"] = "casa";
  $unidad1["id"] = 1;
  $unidad2 = [];
  $unidad2["tipo"] = "departamento";
  $unidad2["id"] = 2;
  $tipos[] = $unidad1;
  $tipos[] = $unidad2;

  $regiones = [];
  $regiones[] = "valparaiso";
  $regiones[] = "metropolitana";

  $fecha = date('Y-m-d H:i:s');


  for($x = 0; $x < count($regiones); $x++){
    for($z = 0; $z < count($tipos); $z++){
      $ch = curl_init('https://www.portalinmobiliario.com/arriendo/' . $tipos[$z]["tipo"] . '/' . $regiones[$x] . '/_Desde_1_OrderId_BEGINS*DESC_NoIndex_True');
      curl_setopt ($ch, CURLOPT_POST, false);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
      curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
      curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
      curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

      $respuesta = curl_exec($ch);

      curl_close($ch);

      $htmlPag = new DOMDocument;
      @$htmlPag->loadHTML($respuesta);

      foreach ($htmlPag->getElementsByTagName('li') as $tagPag) {
        if($tagPag->getAttribute('class') === "andes-pagination__page-count"){
          $vueltas = trim(str_replace('de','',$tagPag->textContent));
        }
      }

      $desde = 1;

      if($vueltas > 10){
        $vueltas = 10;
      }

      for($y = 0; $y < ($vueltas - 2); $y++){
        $ch = curl_init('https://www.portalinmobiliario.com/arriendo/' . $tipos[$z]["tipo"] . '/' . $regiones[$x] . '/_Desde_' . $desde . '_OrderId_BEGINS*DESC_NoIndex_True');
        $desde = $desde + 50;
        curl_setopt ($ch, CURLOPT_POST, false);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

        $respuesta = curl_exec($ch);

        curl_close($ch);

        $html = new DOMDocument;
        @$html->loadHTML($respuesta);

        $unidades = [];

        foreach ($html->getElementsByTagName('ol') as $tag) {
          if($tag->getAttribute('class') === "ui-search-layout ui-search-layout--stack ui-search-layout--pi shops__layout"){
            foreach ($tag->getElementsByTagName('a') as $unidad) {
              if($unidad->getAttribute('class') === "ui-search-result__image ui-search-link"){
                $unidades[] = $unidad->getAttribute('href');
              }
            }
          }
        }

        try{
          for($i = 0; $i < count($unidades); $i++){
            echo $unidades[$i] . "\n";
            $ch = curl_init($unidades[$i]);
            curl_setopt ($ch, CURLOPT_POST, false);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

            $respuesta = curl_exec($ch);

            curl_close($ch);

            // $file = fopen($ruta . 'pagina3.html', 'w+');
            // fwrite($file, $respuesta);
            // fclose($file);

            if($respuesta != ''){
              $html2 = new DOMDocument;
              @$html2->loadHTML($respuesta);

              //Fecha de publicacion
              $d = 0;
              foreach ($html2->getElementsByTagName('p') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-color--GRAY ui-pdp-size--XSMALL ui-pdp-family--REGULAR ui-pdp-header__bottom-subtitle"){
                  $d = trim(explode("día",explode("hace",$tag->textContent)[1])[0]);
                  $fecha_publicacion = date("d-m-Y");
                  $fecha_publicacion = date("Y-m-d",strtotime($fecha_publicacion."- " . $d . " days"));
                }
                if($d == 0){
                  if($tag->getAttribute('class') === "ui-pdp-color--GRAY ui-pdp-size--XSMALL ui-pdp-family--REGULAR ui-pdp-seller-validated__title"){
                    $d = trim(explode("día",explode("hace",$tag->textContent)[1])[0]);
                    $fecha_publicacion = date("d-m-Y");
                    $fecha_publicacion = date("Y-m-d",strtotime($fecha_publicacion."- " . $d . " days"));
                  }
                }
              }

              $MetrosUtiles = 0;
              $MetrosConstruidos = 0;
              $Dormitorios = 0;
              $Banos = 0;
              foreach ($html2->getElementsByTagName('tbody') as $tag) {
                if($tag->getAttribute('class') === "andes-table__body"){
                  foreach ($tag->getElementsByTagName('tr') as $tr) {
                    if($tr->getAttribute('class') === "andes-table__row"){
                      // echo $tr->childNodes->item(0)->textContent . "<br>";
                      // echo $tr->childNodes->item(1)->textContent . "<br>";
                      // echo "------------------<br>";
                      if(trim($tr->childNodes->item(0)->textContent) == "Superficie útil"){
                        $MetrosUtiles = trim(str_replace("m²","",$tr->childNodes->item(1)->textContent));
                        // echo $MetrosUtiles . "<br>";
                        // echo "------------------<br>";
                      }
                      if(trim($tr->childNodes->item(0)->textContent) == "Superficie total"){
                        $MetrosConstruidos = trim(str_replace("m²","",$tr->childNodes->item(1)->textContent));
                        // echo $MetrosConstruidos . "<br>";
                        // echo "------------------<br>";
                      }
                      if(trim($tr->childNodes->item(0)->textContent) == "Dormitorios"){
                        $Dormitorios = trim(str_replace("m²","",$tr->childNodes->item(1)->textContent));
                        // echo $Dormitorios . "<br>";
                        // echo "------------------<br>";
                      }
                      if(trim($tr->childNodes->item(0)->textContent) == "Baños"){
                        $Banos = trim(str_replace("m²","",$tr->childNodes->item(1)->textContent));
                        // echo $Banos . "<br>";
                        // echo "------------------<br>";
                      }
                    }
                  }
                }
              }

              $PrecioUF = 0;
              $PrecioPesos = 0;
              $PrecioDespliegue = 0;
              $IdTipoMoneda = 0;
              $uf = 0;
              foreach ($html2->getElementsByTagName('div') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-price__second-line"){
                  foreach ($tag->getElementsByTagName('span') as $span) {
                    if($span->getAttribute('class') === "andes-money-amount__currency-symbol"){
                      if(trim($span->textContent) == "UF"){
                        $uf = 1;
                      }
                    }
                    if($span->getAttribute('class') === "andes-money-amount__fraction"){
                      if($uf == 1){
                        $PrecioUF = trim(str_replace(".","",$span->textContent));
                        $PrecioDespliegue = "UF " . trim(str_replace(".","",$span->textContent));
                        $IdTipoMoneda = 2;
                        // echo $PrecioUF . "<br>";
                        // echo $PrecioDespliegue . "<br>";
                        // echo "------------------<br>";
                      }
                      else{
                        $PrecioPesos = trim(str_replace(".","",$span->textContent));
                        $PrecioDespliegue = "$ " . trim(str_replace(".","",$span->textContent));
                        $IdTipoMoneda = 1;
                        // echo $PrecioPesos . "<br>";
                        // echo $PrecioDespliegue . "<br>";
                        // echo "------------------<br>";
                      }
                    }
                  }
                }
              }

              $corre = 0;
              $NombreCorredora = '';
              $Nombre = '';
              $Email = '';
              $Telefono = '';
              $TipoCliente = 0;
              $TipoContactoPropiedad = 0;
              foreach ($html2->getElementsByTagName('h2') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-color--BLACK ui-pdp-size--MEDIUM ui-pdp-family--REGULAR ui-vip-seller-profile__header mb-24"){
                  echo trim($tag->textContent) . "\n";
                  if(trim($tag->textContent) != "Información del particular"){
                    $corre = 1;
                  }
                }
              }
              // echo $corre . "<br>";
              foreach ($html2->getElementsByTagName('div') as $tag) {
                if($tag->getAttribute('class') === "ui-vip-profile-info__info-link"){
                  if($corre == 1){
                    $NombreCorredora = trim(str_replace("'","",trim($tag->textContent)));
                    // echo $NombreCorredora . "<br>";
                    // echo "------------------<br>";
                    $TipoCliente = 2;
                    // echo $TipoCliente . "<br>";
                    // echo "------------------<br>";
                    $TipoContactoPropiedad = 2;
                    // echo $TipoCliente . "<br>";
                    // echo "------------------<br>";
                  }
                  else{
                    $Nombre = trim($tag->textContent);
                    // echo $Nombre . "<br>";
                    // echo "------------------<br>";
                    $TipoCliente = 1;
                    // echo $TipoCliente . "<br>";
                    // echo "------------------<br>";
                    $TipoContactoPropiedad = 1;
                    // echo $TipoCliente . "<br>";
                    // echo "------------------<br>";
                  }
                }
              }

              $Observaciones = '';
              foreach ($html2->getElementsByTagName('p') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-description__content"){
                  $Observaciones = trim(str_replace("'","",$tag->textContent));
                  // echo $Observaciones . "<br>";
                  // echo "------------------<br>";
                }
              }

              $Titulo = '';
              foreach ($html2->getElementsByTagName('h1') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-title"){
                  $Titulo = trim(str_replace("'","",trim($tag->textContent)));
                  // echo $Titulo . "<br>";
                  // echo "------------------<br>";
                }
              }

              $Comuna = '';
              foreach ($html2->getElementsByTagName('div') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-media ui-vip-location__subtitle ui-pdp-color--BLACK"){
                  foreach ($tag->getElementsByTagName('p') as $tag) {
                    $Comuna = trim(explode(",", $tag->textContent)[count(explode(",", $tag->textContent)) - 2]);
                    // echo $Comuna . "<br>";
                    // echo "------------------<br>";
                  }
                }
              }

              $Id = '';
              $CodigoPropiedad = '';
              foreach ($html2->getElementsByTagName('span') as $tag) {
                if($tag->getAttribute('class') === "ui-pdp-color--BLACK ui-pdp-family--SEMIBOLD"){
                  $Id = trim($tag->textContent);
                  $CodigoPropiedad = $Id;
                }
              }

              $EstaPublicada = 1;
              $EstaEliminada = 0;
              $EsHabitacional = 1;
              $FechaPublicacionDespliegue = $fecha_publicacion;
              $MetrosTerraza = 0;
              $MetrosTerreno = 0;
              $AñoConstruccion = 0;
              $TipoOperacion = "Arriendo";
              $IdEstadoPropiedad = 2;
              $UriFicha = $unidades[$i];
              $IdTipoPropiedad = $tipos[$z]["id"];
              $DireccionCalle = '';
              $DireccionDepto = '';
              $DireccionNumero = '';
              $Region = $regiones[$x];
              $FechaCarga = $fecha;

              if($Id != ''){
                $row = ingresaInmueble($EstaPublicada,$EstaEliminada,$EsHabitacional,$FechaPublicacionDespliegue,$MetrosUtiles,$MetrosTerraza,$MetrosConstruidos,$MetrosTerreno,$Dormitorios,$Banos,$PrecioUF,$PrecioPesos,$AñoConstruccion,$TipoCliente,$Id,$IdEstadoPropiedad,$IdTipoMoneda,$TipoContactoPropiedad,$UriFicha,$Observaciones,$CodigoPropiedad,$NombreCorredora,$PrecioDespliegue,$Titulo,$DireccionCalle,$DireccionDepto,$DireccionNumero,$Comuna,$Region,$TipoOperacion,$Nombre,$Email,$Telefono,$IdTipoPropiedad,$FechaCarga);

                if($row == "Ok"){
                  echo "Ingresado correctamente: " . $Id . "\n";
                  echo "---------------------------------\n";
                }
                else{
                  var_dump($row);
                  // echo "Error de ingreso: " . $propiedad['Id'] . "\n";
                }

                sleep(1);
              }
            }
          }
        }
        catch (Exception $e){
          echo "Error de ingreso\n";
        }
      }
    }
  }
?>
